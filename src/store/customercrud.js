import axios from "axios";

export default {
  namespaced: true,

  state: {
    customerData: null
  },

  getters: {
    customer: state => state.customerData
  },

  mutations: {
    setCustomerData(state, customer) {
      state.customerData = customer;
    }
  },

  actions: {
    getCustomerData({ commit }) {
      axios
        .get(process.env.VUE_APP_API_URL + "user/customers")
        .then(response => {
          console.log("here" + response.data);
          commit("setCustomerData", response.data);
        });
    }
  }
};
