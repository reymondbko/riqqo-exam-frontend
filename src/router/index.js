import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const guest = (to, from, next) => {
  if (!localStorage.getItem("authToken")) {
    return next();
  } else {
    return next("/home");
  }
};

const routes = [
  {
    path: "/home",
    name: "Home",
    component: () => import(/* webpackChunkName: "home" */ "../views/Home.vue")
  },
  {
    path: "/",
    name: "Login",
    beforeEnter: guest,
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Auth/Login.vue")
  },
  {
    path: "/customers",
    name: "Customers",
    component: () =>
      import(/* webpackChunkName: "home" */ "../views/Customers.vue")
  },
  {
    path: "/add",
    name: "add",
    component: () =>
      import(/* webpackChunkName: "home" */ "../views/AddCustomer.vue")
  },
  {
    path: "/edit/:id",
    name: "edit",
    component: () =>
      import(/* webpackChunkName: "home" */ "../views/EditCustomer.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
