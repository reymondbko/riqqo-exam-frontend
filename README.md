# front
## Configuration
 Clone this project or Download that ZIP file
`On your terminal/command prompt do the following commands.

- Navigate to the project directory
```
$ cd <project-directory>
```  
- Copy .env.localexample and rename to .env.local, then open .env.local and replace VUE_APP_API_URL=your_backend_url/api/auth
```
`$ cp .env.localexample .env.local`  
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
